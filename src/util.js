import Big from 'big.js';

/**
 * Mithril buildQueryString Utility
 * Adapted from https://github.com/MithrilJS/mithril.js/blob/next/querystring/build.js
 * @param {Object} object 
 */
export const buildQuery = object => {
    if (Object.prototype.toString.call(object) !== '[object Object]') {
        return '';
    }

    const args = [];
    for (const key in object) {
        destructure(key, object[key]);
    }

    return args.join('&');

    function destructure(key, value) {
        if (Array.isArray(value)) {
            for (let i = 0; i < value.length; i++) {
                destructure(key + '[' + i + ']', value[i]);
            }
        } else if(Object.prototype.toString.call(value) === '[object Object]') {
            for (const i in value) {
                destructure(key + '[' + i + ']', value[i]);
            }
        } else {
            args.push(encodeURIComponent(key) + (value != null && value !== '' ? '=' + encodeURIComponent(value) : ''))
        }
    }
};

/**
 * Convert ID Utility
 * @param {String} format 
 * @param {String} id 
 */
export const convertId = (format, id) => {
    const base = '76561197960265728';

    switch(format) {
        case 'to32':
            return new Big(id).minus(base).toString();
        case 'to64':
            return new Big(id).minus(base).toString();
        default:
            return id;
    }
};