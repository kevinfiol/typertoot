import { SET_CURRENTWORD } from './actionTypes';

export default function(state, action) {
    switch(action.type) {
        case SET_CURRENTWORD: {
            const { currentWord } = action.payload;
            return Object.assign({}, state, {
                currentWord
            });
        }

        default:
            return state;
    }
}