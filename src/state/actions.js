import { SET_CURRENTWORD } from './actionTypes';

export default (update, reduce) => ({
    setCurrentWord: currentWord => update(state => {
        return reduce(state, {
            type: SET_CURRENTWORD,
            payload: { currentWord }
        });
    })
});