import { redraw as redrawFunction } from 'mithril';
import stream from 'mithril/stream';
import initialState from './initialState';
import reducerFunction from './reducer';
import createActions from './actions';
import createAsyncActions from './asyncActions';

const update = stream();
const state  = stream.scan((x, f) => f(x), initialState, update);

const actions      = createActions(update, reducerFunction);
const asyncActions = createAsyncActions(actions, redrawFunction);
const allActions   = Object.assign({}, actions, asyncActions);

export { state, allActions as actions };