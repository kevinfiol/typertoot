import prompts from '../data/prompts.json';

const prompt = prompts['prompt_1'];
const promptArr = prompt.split(' ');
const promptLen = promptArr.length;

const initialState = {
    prompt,
    promptArr,
    promptLen,
    currentWord: 0
};

export default initialState;