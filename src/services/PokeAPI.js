import fetch from 'unfetch';

const baseUrl = 'https://pokeapi.co/api/v2';
const options = { method: 'GET', headers: { 'Content-Type': 'application/json' } };

export const getPokemon = identifier => {
    return fetch(`${baseUrl}/pokemon/${identifier}`, options)
        .then(r => r.json())
    ;
};