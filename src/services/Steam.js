import fetch from 'unfetch';

const baseUrl = 'https://sfn.herokuapp.com';
const userEndpoint = `${baseUrl}/steamAPI/ISteamUser`;

const defaultOptions = { method: 'POST', headers: { 'Content-Type': 'application/json' } };
const createOptionsWithParams = params => Object.assign({}, defaultOptions, { body: JSON.stringify(params) });

export const resolveVanityURL = vanityurl => {
    const options = createOptionsWithParams({ vanityurl });
    return fetch(`${userEndpoint}/ResolveVanityURL/v0001/`, options)
        .then(r => r.json())
        .then(JSON.parse)
        .then(data => {
            if (data.response.success === 1) return data.response.steamid;
            else throw new Error('Invalid Vanity ID');
        })
    ;
};

export const getFriendList = steamid => {
    const options = createOptionsWithParams({ steamid, relationship: 'friend' });
    return fetch(`${userEndpoint}/GetFriendList/v0001/`, options)
        .then(r => r.json())
        .then(JSON.parse)
        .then(data => {
            if (data && Object.keys(data).length) return data.friendslist.friends;
            else throw new Error('Invalid ID');
        })
    ;
};

export const getPlayerSummaries = steamids => {
    const options = createOptionsWithParams({ steamids });
    return fetch(`${userEndpoint}/GetPlayerSummaries/v0002/`, options)
        .then(r => r.json())
        .then(JSON.parse)
        .then(data => {
            if (data && Object.keys(data).length) return data.response.players;
            else throw new Error('Unable to retrieve player summaries');
        })
    ;
};