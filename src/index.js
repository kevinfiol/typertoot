import m from 'mithril';
import App from 'components/Typer/App';

import 'ace-css/css/ace.min.css';
import './styles/main.css';

import { state, actions } from 'state';

m.mount(document.getElementById('app'), {
    view: () => m(App, { state: state(), actions })
});