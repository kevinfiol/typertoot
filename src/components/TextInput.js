import m from 'mithril';

const StyledInput = 'input.input';

const TextInput = {
    view: ({attrs}) => m(StyledInput, Object.assign({ type: 'text' }, attrs))
};

export default TextInput;