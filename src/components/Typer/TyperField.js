import m from 'mithril';
import TextInput from 'components/TextInput';

const TyperField = () => {
    let input;
    let onTextInput;
    let onKeyPress;

    return {
        oninit: ({ attrs: { state, actions } }) => {
            input = '';

            onTextInput = e => input = e.target.value;

            onKeyPress = (e, currentWord) => {
                if (e.code === 'Space' || e.keyCode === 32) {
                    console.log('press space');
                    console.log('current input', input);
                    console.log('current word', state.promptArr[currentWord]);
                    console.log('match ?', input.trim() === state.promptArr[currentWord]);

                    if (input.trim() === state.promptArr[currentWord]) {
                        input = '';
                        console.log('clear input');
                    }
                }
            };
        },

        view: ({ attrs: { state } }) => m('div',
            m(TextInput, {
                oninput: onTextInput,
                onkeyup: e => onKeyPress(e, state.currentWord),
                value: input
            }),
            m('p', state.prompt),
            m('p', state.currentWord)
        )
    };
};

export default TyperField;