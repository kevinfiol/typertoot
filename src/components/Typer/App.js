import m from 'mithril';
import TyperField from './TyperField';

const StyledDiv = '.max-width-3.mx-auto.py3';

const App = {
    view: ({ attrs: { state, actions } }) => m(StyledDiv,
        m(TyperField, { state, actions })
    )
};

export default App;