import m from 'mithril';

const StyledTextArea = 'textarea.textarea';

const TextArea = {
    view: ({attrs}) => m(StyledTextArea, attrs)
};

export default TextArea;