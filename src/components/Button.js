import m from 'mithril';

const StyledButton = 'button.btn';

const Button = {
    view: ({attrs, children}) => m(StyledButton, attrs, children)
};

export default Button;