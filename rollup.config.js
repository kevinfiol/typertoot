import path from 'path';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import buble from 'rollup-plugin-buble';
import uglify from 'rollup-plugin-uglify';
import alias from 'rollup-plugin-alias';
import postcss from 'rollup-plugin-postcss';
import json from 'rollup-plugin-json';

const aliases = {
    'components': path.resolve(__dirname, 'src/components'),
    'services': path.resolve(__dirname, 'src/services'),
    'state': path.resolve(__dirname, 'src/state/index'),
    'util': path.resolve(__dirname, 'src/util')
};

const config = {
    input: './src/index.js',
    output: {
        file: './public/app.js',
        format: 'iife',
        sourcemap: true
    },
    plugins: [
        json({ exclude: 'node_modules/**', preferConst: true }),
        postcss(),
        nodeResolve(),
        commonjs(),
        alias(aliases),
        buble()
    ],
};

if (process.env.PROD === 'true') {
    config.output.sourcemap = false;
    config.plugins.push( uglify.uglify() );
}

export default config;